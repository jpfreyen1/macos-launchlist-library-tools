//
//  launchlistUIApp.swift
//  launchlistUI
//
//  Created by Jay Freyensee on 1/17/22.
//

import SwiftUI

@main
struct launchlistUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
