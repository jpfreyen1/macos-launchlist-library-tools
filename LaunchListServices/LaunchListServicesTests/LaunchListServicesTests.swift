//
//  LaunchListServicesTests.swift
//  LaunchListServicesTests
//
//  Created by Jay Freyensee on 2/22/22.
//

import XCTest
@testable import LaunchListServices

class LaunchListServicesTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // more setup code goes here
    }
    
    override func tearDown() {
        super.tearDown()
        // more tearDown code goes here
    }
    
    func testEmptyVerList() {
        let ls = LaunchListServices()
        XCTAssertEqual(ls.getAppVersion(""), "",
                       "pass in \"\", pass out \"\" for getAppVersion()")
    }
    
    func testListSortReady() {
        let ls = LaunchListServices()
        let dict = ["": ""]
        var array =   Array<(key: String, value: String)>()
        array = ls.listSortReady(dict)
        XCTAssertEqual(dict[""], array[0].key, "empty list condition for listSortReady()")
        
        let nullList =  ["/a/bc": "(null)",
                         "/v/wah/you": "4.8",
                         "/a/bc/Xtra/special": "(null)",
                         "/Too/Legit/to/Quit": "2.145 (345)"]
        let sortedList = ls.listSortReady(nullList)
        
        XCTAssertEqual(sortedList[0].value, "(null)")
        XCTAssertEqual(sortedList[1].value, "(null)")
        XCTAssertEqual(sortedList[2].value, "2.145 (345)")
        XCTAssertEqual(sortedList[3].value, "4.8")
    }
    
    func testGetAllVersionsFromApps() {
        let ls = LaunchListServices()
        let result = ls.getAllVersionsFromApps([""], false)
        XCTAssertEqual(result[""], "")
    }
    
    /*
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
    */
    
}
