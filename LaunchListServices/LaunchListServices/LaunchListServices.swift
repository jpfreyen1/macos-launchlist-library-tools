//
//  launchListServices.swift
//
//  Created by Jay Freyensee on 2/7/22.
//
// This is a library intended to interface to the Launch Services
// subsystem for data. It can be used for command-line or UI app.
//
// To create a static shared library for swift and macOS isn't straightforward
// as it should be:
//
// 1. Create an iOS Cocoa Touch Static Library project.
//    For example, name it MyStaticLib. Choose Swift for its Language.
//
// 2. Modify the Build Settings of the target MyStaticLib as follows:
//    Supported Platforms: iOS → macOS
//    Base SDK: Latest iOS → Latest macOS
// 3. Add any classes and methods or any types you need and build it.
//    (make them public scope or it won't work)
// 4. Find libMyStaticLib.a (not red, when successfully built)
//    in the Products group of the Project navigator of your Xcode.
//


import Foundation
import OSLog
import SwiftUI

fileprivate let logger = Logger(subsystem: "com.rocksolid.libLaunchListServices",
                                category: "PatchMgmt")

/// logerr(): Really, we only want to use macOS's oslog and console  for apps,
/// command-line binaries printing to the terminal works great regardless
/// if its a DEBUG or RELEASE build.
///
/// - Parameters:
///   - expression: string to print to stdout or macOS's oslog and console.
fileprivate func logerr(_ expression: String) {
    #if USE_WITH_UI
    logger.error("\(expression, privacy: .public)")
    #else
    print("\(expression)")
    #endif
}

/// loginfo() is more of pure debugging and we don't
/// really want to see this in Release regardless
/// if its command-line.
///
/// - Parameters:
///   - expression: string to print to stdout or macOS's oslog.
fileprivate func loginfo(_ expression: String) {
    #if DEBUG
    print("\(expression)")
    
    #else
    // if we want to use a "log collect" command and then look at results
    // via console or "log show  --archive" use logger.info() over
    // logger.notice(). logger.info destination is to memory
    // (so won't be around long) whereas logger.notice() goes to disk.
    // logger.notice() is supposed to be default.
    // logger.info("\(expression, privacy: .public)")
    logger.notice("\(expression, privacy: .public)")
    #endif
}

// A hidden command in macOS used to
// query launch services db.  You can get
// man pages by typing THE COMPLETE PATH NAME
// (only absolute pathname works)
fileprivate enum lsregisterApp {
    static let CMD = "/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister"
    static let DUMP_ARG  = "-dump"
}

fileprivate enum mdlsVersion {
    static let CMD = "/usr/bin/mdls"
    static let ARGS = ["-name", "kMDItemVersion"]
}

fileprivate enum grepFindApp {
    static let CMD = "/usr/bin/grep"
    static let ARGS = ["-F", ".app "]
}

/// This is a library intended to interface to the Launch Services
/// subsystem for data. It can be used for command-line or UI app.
public struct LaunchListServices {
    
    /// Just in case it's tedious to get the man pages on the command-line,
    /// (it's not as trivial as you think it is), this functions retrieves the lsregister
    /// man pages.
    /// - Returns: the manpage for lsregister.
    public func manpages() -> String {
        let manpage = Process()
        let errPipe = Pipe()
        
        manpage.standardError = errPipe
        manpage.executableURL = URL(fileURLWithPath: lsregisterApp.CMD)
        
        do {
            try manpage.run()
            let errHandle = errPipe.fileHandleForReading.readDataToEndOfFile()
            let err       = String(decoding: errHandle, as: UTF8.self)
            return err
        }
        catch let manError {
            logerr("Error: retrieving manpage of lsregister: \(manError)")
            return "Error: retrieving manpage of lsregister: \(manError)"
        }
    }
    
    /// default implementation that has to be public because of the struct
    /// implentation being in a static library.
    public init() {}
    
    /// print .app-version results to log (console).  See loginfo() for more
    /// about macOS logging.
    ///  - Parameters:
    ///   - expression: string to print to console (log).
    public func printListToConsoleLog(_ expression: [String:String]) -> Void {
        for (k, v) in expression {
            logger.notice("\(k, privacy: .public): \(v, privacy: .public)")
        }
    }
    
    /// Mainly a debug function for getAppList(), using loginfo()
    /// will send to oslog macOS subsystem.
    /// - Parameters:
    ///   - list: list of absolute pathnames of apps.
    /// - Returns: nothing, prints result.
    public func printAppList(_ list: [String]) -> Void {
        for i in list {
            loginfo(i)
        }
    }
    
    /// Mainly a debug function for getAllVersionsFromApps(),
    /// print out the versions for each app.
    /// - Parameters:
    ///   - dict: dictionary list containing absolute pathname (key) and version (value).
    public func printAppVerList(_ dict: [String:String]) -> Void {
        for (k, v) in dict {
            loginfo("\(k): \(v)")
        }
    }
    
    /// Retrieves all .app registered to Launch Services subsystem.
    ///
    /// - Returns: a list of strings, absolute pathnames of .app.
    public func getAppList() -> [String] {

        let CHOPPATH = "path:"
        let dbPipe   = Pipe()
        let grepPipe = Pipe()
        let appStream = Process()
        let grepFind  = Process()
        
        appStream.standardOutput = dbPipe
        appStream.executableURL = URL(fileURLWithPath: lsregisterApp.CMD)
        appStream.arguments = [lsregisterApp.DUMP_ARG]
        grepFind.standardInput  = dbPipe
        grepFind.standardOutput = grepPipe
        grepFind.executableURL  = URL(fileURLWithPath: grepFindApp.CMD)
        grepFind.arguments = [grepFindApp.ARGS[0], grepFindApp.ARGS[1]]
        
        var appList     = [String]()
        var tidyAppList = [String]()
        do {
            try appStream.run()
            try grepFind.run()
            
            let outH = grepPipe.fileHandleForReading.readDataToEndOfFile()
            appList = String(decoding: outH, as:UTF8.self).components(separatedBy: .newlines)
            
            // when filtering the contents of the launchservices database
            // dump there will be two types of lines left: "path: " and
            // "bundle id:".  "bundle id:" is useless, it only has the .app
            // name, no absolute path we need.
            //
            // Thus:
            // 1. eliminate "bundle id:" lines
            //
            // 2. beautify the path lines.  remove the characters
            // "path:" at each string beginning, a leftover from the database.
            // Also get rid of hex number at the end of each line.
            // We look for the "(0" as our line separator, example (0x123c),
            // to trim the hex number off as we have no idea how many
            // spaces separate between the ".app" ending and hex number.
            for i in appList {
                if (i.hasPrefix(CHOPPATH)) {
                    let choppedPath = String(i.dropFirst(CHOPPATH.count))
                    let hexTrimmed = choppedPath.components(separatedBy: "(0")
                    tidyAppList.append(hexTrimmed[0].trimmingCharacters(in: .whitespaces))
                }
            }
            return tidyAppList
            
        }
        catch {
            logerr("Error: no applist retrieved: \(error)")
            return["Error: no applist retrieved: \(error)"]
        }
    }
    
    ///  A function that will conver a [String:String] map to a Array<(key: String, value: String)>
    ///  for sorting the mapped data for things like writing to a csv file.
    /// - Parameters
    ///   - appVerList: a unsorted dictionary.
    ///
    /// - Returns: a sorted dictionary arrary in the format of Array<(key: String, value: String)>
    ///            such that all the 'null' versions are all together.  To be used as a List() for things like
    ///            using createAppCSV() in CsvFormatHelper to write out to a csv file or
    ///            in a list in SwiftUI.
    ///
    public func listSortReady(_ appVerList: [String: String]) -> Array<(key: String, value: String)> {
        let sorted = appVerList.sorted() {
            $0.value < $1.value
        }
        return sorted
    }
    
    /// Test function to figure out how to relate key and value and print them out per line per list
    /// entry when setting up a Swift UI to display the app info
    public func printKVArray(_ array: Array<(key: String, value: String)>) -> Void {
        for (i,_) in array.enumerated() {
            print("\(array[i].key) ==> \(array[i].value)")
        }
    }
    /// This will take a dict array and transform it into a two member element list:
    ///  app
    ///  version
    /// and return it sorted based on the version (or lack of version)
    /// this should be useful for writing swift UIs, like presenting the list in a GUI
    /// - Parameters:
    ///   - rawdict: [string : string] to use to make a two member element list
    /// - Returns: [app:string, version:string][] array, sorted such that the 'nulls' in version
    ///            are grouped together in one piece.
    /*func spitOutAppVerArray(_ rawdict: [String:String]) ->  Array<(key: String, value: String)> {
        var appverList: Array<String, String> = []
        
        return appverList;
    }*/
    
    /// This will retrieve all versions associated with a list of all apps
    /// - Parameters:
    ///   - applist:  absolute pathname of all .apps  (in a list) we want to retrieve the versions
    ///   - numbers:  retrieve only .apps with numbered versions. So say /a/b/my.app
    ///               has "(null)" for the version, /a/b/my.app will not be a part
    ///               of the returned dictionary if true
    ///
    /// - Returns: a un-ordered map (or dictionary) where the 'key' is the
    ///            absolute .app pathname and 'value' is the version.
    public func getAllVersionsFromApps(_ applist: [String], _ numbers: Bool)
    -> [String: String] {
        
        var appVerDict = [String : String]()
        for i in applist {
            let appVer = getAppVersion(i)
            if (numbers == true) {
                
                // we want to get rid of the two states mdls can return,
                // null and our defined (more user friendly statement)
                // which we supplied the substring "no longer"
                if ( (!appVer.contains("null")) &&
                     (!appVer.contains("no longer")) ) {
                    appVerDict[i] = appVer.trimmingCharacters(in: .newlines)
                    appVerDict[i] = appVerDict[i]?.replacingOccurrences(of: "\"", with: "")
                }
            } else {
                appVerDict[i] = appVer.trimmingCharacters(in: .newlines)
                appVerDict[i] = appVerDict[i]?.replacingOccurrences(of: "\"", with: "")
            }
        }
        return appVerDict
    }
    
    /// This uses the spotlight command-line mdls to retrieve the version of a given app.
    /// - Parameters:
    ///  - pathname: absolute pathname of app.
    ///
    /// - Returns: version of app as a string.  If pathname is empty
    ///            return empty ("").
    public func getAppVersion(_ pathname: String) -> String {
        
        if (pathname.isEmpty) {
            return ""
        }
        
        let appverPipe = Pipe()
        let spotlightMlds = Process()
        spotlightMlds.standardOutput = appverPipe
        spotlightMlds.executableURL = URL(fileURLWithPath: mdlsVersion.CMD)
        spotlightMlds.arguments = [mdlsVersion.ARGS[0], mdlsVersion.ARGS[1], pathname]
        
        // for the case format: KMDItemVersion = (null)
        // there is no version associated with that app.  It can happen, my
        // own xcode compiled apps have been flagged with no version.
        
        var resultingVersion = [String]()
        let fm = FileManager();
        do {
            
            // a couple of cases here:
            // 1. resultingVersion being an array of 2
            //    (happens even if KMDItemVersion = (null))
            // 2. or mlds plain fails which there will not be an array length of 2
            //
            // if 2., then it means there is a record of an .app
            // installed on the system but it's no longer on the computer.
            // It actually seems to be normal working behavior
            // (per Apple engineer).
            // I believe the launch services db can be updated/rebuilt,
            // but more research is need (and Apple engineer didn't think it was
            // a good idea to rebuild/refresh launch services db).
            if (fm.fileExists(atPath: pathname)) {
                try spotlightMlds.run()
                let output = appverPipe.fileHandleForReading.readDataToEndOfFile()
                
                resultingVersion = String(decoding: output,
                                          as:UTF8.self).components(separatedBy: "=")
            }
        }
        catch {
            logerr("Error: no version retrieved of \(pathname): \(error)")
            return "Error: no version retrieved of \(pathname): \(error)"
        }
        
        if (resultingVersion.count == 2) {
            return resultingVersion[1]
        }
        else {
            return "It appears there is a record of \(pathname) installed on the system but it's no longer on the computer!"
        }
    }
}

