//
//  CsvFormatHelper.swift
//
//
//  Created by Jay Freyensee on 7/31/21.
//
//  Alogrithm based on stackoverflow:
// https://stackoverflow.com/questions/55870174/how-to-create-a-csv-file-using-swift
//

import Foundation

struct CsvFormatHelper {
    
    let FILENAME = "lauchlistapps.csv"
    let SEPERATOR = "|"
    
    //
    /// Create the csv contents, each entry separated by 'SEPERATOR'
    /// - Parameters:
    ///    - appArray: List where each index has a key and value entry
    ///    
    public func createAppCSV(_ appArray: Array<(key: String, value: String)>) {
        var csvString = "\(".App")\(SEPERATOR)\("Version")\n\n"
        
        for (item, value) in appArray {
            csvString = csvString.appending("\(item)\(SEPERATOR)\(value)\n");
        }
        
        let fileManager = FileManager.default
        do {
            let path = try fileManager.url(for: .desktopDirectory, in: .allDomainsMask, appropriateFor: nil, create: false)
            let fileURL = path.appendingPathComponent(FILENAME)
            try csvString.write(to: fileURL, atomically: true, encoding: .utf8)
            print("\(fileURL.path) created.")
            print(".csv delimiter used: \'\(SEPERATOR)\'")
        } catch {
            print("Error: Problem creating \(FILENAME)")
        }
    }
}
