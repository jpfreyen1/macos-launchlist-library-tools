//
//  main.swift
//  lauchlistapps- command-line tool to read from
//                 launch services subsystem to get
//                 all apps that are in theory installed
//                 on the macOS machine (macOS packages
//                 from 3rd party open source may be an exception)...
//                 very least it gets everything installed and allowed
//                 to "launch"
//
//
//  Created by Jay Freyensee on 1/17/22.
//

import Foundation
import LaunchListServices

var justNumbers : Bool = true

let LSREGISTER : String      = "--lsregister-manpage"
let FORENSIC_SWITCH : String = "--forensic-mode"
let HELP : String            = "--help"
let HELP_COMMENT = """
\(FORENSIC_SWITCH): Retreive all apps on system even if they don't have a
  version associated with it. For macOS, this could be the version of an .app was
  null (designer never assigned a version) or the .app is in limb with the
  recycling bin (like its staged for delete but the recycling bin isn't
  empty).  Default mode only retrieves .app with versions (numbers).\n
\(LSREGISTER): Print out the manpage for lsregister.
"""

let lls = LaunchListServices()

for arg in CommandLine.arguments {
    switch arg {
    case FORENSIC_SWITCH:
        justNumbers = false
    case LSREGISTER:
        print(lls.manpages())
        exit(1)
    case HELP:
        print("\n\(HELP_COMMENT)\n")
        exit(2)
    default:
        break
    }
}


let list = lls.getAppList()
let verlist = lls.getAllVersionsFromApps(list, justNumbers)
let readyForCsv = lls.listSortReady(verlist)

let csv = CsvFormatHelper()
csv.createAppCSV(readyForCsv)

lls.printListToConsoleLog(verlist)
